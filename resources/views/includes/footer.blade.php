<!-- Footer -->
<footer class="sticky-footer bg-secondary">
    <div class="container my-auto">
        <div class="copyright text-center text-light my-auto">
            <span>Copyright &copy; Your Website 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->