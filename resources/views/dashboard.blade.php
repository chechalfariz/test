<x-template-layout>
    <x-slot name="header">
        <h2 class="h4 text-dark">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="container-fluid">
        <div class="card shadow-lg">
            <div class="card-body text-dark text-center">
                <b class="h2">
                    {{ __("You're logged in!") }}
                </b>
            </div>
        </div>
    </div>
</x-template-layout>