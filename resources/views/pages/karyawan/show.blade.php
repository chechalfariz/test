<x-template-layout>
    <x-slot name="header">
        {{ __('Detail Karyawan') }}
    </x-slot>
    <div class="container-fluid d-flex justify-content-center mb-4">
        <div class="card shadow-lg border-left-warning" style="width: 25rem;">
            <div class="card-header">
                Card Information Karyawan {{ $karyawan->id }}
            </div>
            <img src="{{ $karyawan->photo_url }}" class="card-image-top" style="height: 20rem;"
                alt="{{ $karyawan->photo_url }}">
            <div class="card-body">
                <h5 class="card-title shadow p-2 text-dark text-center"><b>{{ $karyawan->name }}</b></h5>
                <table class="table">
                    <tr>
                        <th>Tanggal Lahir</th>
                        <td>:</td>
                        <td>{{ $karyawan->tanggal_lahir }}</td>
                    </tr>
                    <tr>
                        <th>Umur</th>
                        <td>:</td>
                        <td>{{ $umur }}</td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>:</td>
                        <td>{{ $karyawan->alamat }}</td>
                    </tr>
                    <tr>
                        <th>Status Pernikahan</th>
                        <td>:</td>
                        <td>{{ $karyawan->status ? 'Menikah' : 'Lajang' }} </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</x-template-layout>