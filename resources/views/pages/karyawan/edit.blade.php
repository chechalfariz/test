<x-template-layout>
    <x-slot name="header">
        {{ __('Karyawans') }}
    </x-slot>

    <div class="container-fluid">
        <div class="card mb-4">
            <div class="card-header">
                <h3>Edit Karyawan</h3>
            </div>
            <form action="{{ route('karyawan.update', $karyawan->id) }}" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    @method('PUT')
                    <div>
                        <x-input-label for="karyawan_id" :value="__('Karyawan')" />
                        <x-text-input id="name" name="name" type="text" class="mt-1 block w-full"
                            :value="old('name', $karyawan->name)" />
                        <x-input-error class="mt-2" :messages="$errors->get('name')" />
                    </div>
                    <div>
                        <x-input-label for="tanggal_lahir" :value="__('Tanggal Lahir')" />
                        <x-text-input id="tanggal_lahir" name="tanggal_lahir" type="date" class="mt-1 block w-full"
                            :value="old('tanggal_lahir', $karyawan->tanggal_lahir)" />
                        <x-input-error class="mt-2" :messages="$errors->get('tanggal_lahir')" />
                    </div>
                    <div>
                        <x-input-label for="alamat" :value="__('Alamat')" />
                        <x-text-input id="alamat" name="alamat" type="text" class="mt-1 block w-full"
                            :value="old('alamat', $karyawan->alamat)" />
                        <x-input-error class="mt-2" :messages="$errors->get('alamat')" />
                    </div>
                    <div>
                        <x-input-label for="photo_url" :value="__('Ganti Gambar')" />
                        <x-text-input id="photo_url" name="photo_url" type="file" class="mt-1 block w-full"
                            :value="old('photo_url')" />
                        <x-input-error class="mt-2" :messages="$errors->get('photo_url')" />
                    </div>
                    <div>
                        <input name="old_url" type="hidden" value="{{ old('photo',$karyawan->photo_url) }}">
                        <div class="card mt-2 mb-4">
                            <div class="card-header d-flex justify-content-center">
                                <img src="{{ $karyawan->photo_url }}" class="card-image-top img-fluid mr-4"
                                    style="height: 10rem;" alt="{{ $karyawan->photo_url }}">
                            </div>
                        </div>
                    </div>
                    <div>
                        <x-input-label for="status" :value="__('Sudah Menikah ?')" />
                        <x-text-input type="hidden" name="status" :value="0" />
                        <input type="checkbox" id="status" name="status" value="1" {{ old('status', $karyawan->status) ? 'checked' : '' }} class="mt-1 block w-full mb-2 form-control">
                        <x-input-error class="mt-2" :messages="$errors->get('status')" />
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-success">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</x-template-layout>