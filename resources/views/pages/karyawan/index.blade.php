<x-template-layout>
    <x-slot name="header">
        {{ __('Karyawans') }}
    </x-slot>

    <div class="container-fluid">
        <div class="card mb-4 shadow-lg">
            <div class="card-header d-flex justify-content-between align-items-center">
                <h3>List Karyawan</h3>
                <a href="{{ route('karyawan.create') }}" class="btn btn-success">
                    +
                </a>
            </div>
            <div class="card-body">
                <table id="karyawanTable" class="table table-hover text-center">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Karyawan</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Photo</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody><tbody>
                </table>
            </div>
        </div>
    </div>

    <x-slot name="script">
        <script>
            $(document).ready(function() {
                var datatable = $('#karyawanTable').DataTable({
                    ajax: {
                        url: '{!! url()->current() !!}'
                    },
                    columns: [
                        { data: 'id', name: 'id', width: '5%' },
                        { data: 'name', name: 'name' },
                        { data: 'tanggal_lahir', name: 'tanggal_lahir' },
                        { data: 'alamat', name: 'alamat' },
                        { 
                            data: 'photo_url', 
                            name: 'photo_url',
                            render: function(data, type, row) {
                                var defaultImage = '{{ asset('template/img/sql-server.jpg') }}';
                                var imageUrl = data ? data :defaultImage;
                                return '<img src="' + imageUrl + '" alt="photo" style="width: 80px;" class="shadow-lg">';
                            } 
                        },
                        { data: 'status', name: 'status' },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: true,
                            width: '25%'
                        }
                    ]
                });
            })
        </script>
    </x-slot>
    
</x-template-layout>
