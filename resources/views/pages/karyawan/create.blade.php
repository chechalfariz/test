<x-template-layout>
    <x-slot name="header">
        {{ __('Karyawans') }}
    </x-slot>

    <div class="container-fluid">
        <div class="card mb-4">
            <div class="card-header">
                <h3>Tambah Karyawan</h3>
            </div>
            <form action="{{ route('karyawan.store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    <div>
                        <x-input-label for="karyawan_id"  :value="__('Karyawan')" />
                        <x-text-input id="name" name="name" type="text" class="mt-1 block w-full"
                            :value="old('name')" />
                        <x-input-error class="mt-2" :messages="$errors->get('name')" />
                    </div>
                    <div>
                        <x-input-label for="tanggal_lahir" :value="__('Tanggal Lahir')" />
                        <x-text-input id="tanggal_lahir" name="tanggal_lahir" type="date" class="mt-1 block w-full"
                            :value="old('tanggal_lahir')" />
                        <x-input-error class="mt-2" :messages="$errors->get('tanggal_lahir')" />
                    </div>
                    <div>
                        <x-input-label for="alamat" :value="__('Alamat')" />
                        <x-text-input id="alamat" name="alamat" type="text" class="mt-1 block w-full"
                            :value="old('alamat')" />
                        <x-input-error class="mt-2" :messages="$errors->get('alamat')" />
                    </div>
                    <div>
                        <x-input-label for="photo_url" :value="__('Upload Gambar')" />
                        <x-text-input id="photo_url" name="photo_url" type="file" class="mt-1 block w-full"
                            :value="old('photo_url')" />
                        <x-input-error class="mt-2" :messages="$errors->get('photo_url')" />
                    </div>
                    <div>
                        <x-input-label for="status" :value="__('Sudah Menikah ?')" />
                        <x-text-input id="status" name="status" type="checkbox" class="mt-1 block w-full mb-2"
                            :value="1"/>
                        <x-input-error class="mt-2" :messages="$errors->get('status')" />
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-success">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</x-template-layout>