<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\Karyawan;

class KaryawanSeeder extends Seeder
{
    public function run()
    {
        // Paths ke gambar lokal
        $localImagePath1 = base_path('public/template/img/photo1.png');
        $localImagePath2 = base_path('public/template/img/sql-server.jpg');
        
        // Path penyimpanan di storage
        $destinationPath = 'public/images';
        
        // Simpan gambar pertama ke storage dan dapatkan path-nya
        if (file_exists($localImagePath1)) {
            $path1 = Storage::putFileAs($destinationPath, new \Illuminate\Http\File($localImagePath1), Str::random(10) . '.png');
        } else {
            $path1 = null;
        }

        // Simpan gambar kedua ke storage dan dapatkan path-nya
        if (file_exists($localImagePath2)) {
            $path2 = Storage::putFileAs($destinationPath, new \Illuminate\Http\File($localImagePath2), Str::random(10) . '.jpg');
        } else {
            $path2 = null;
        }

        // Buat karyawan pertama dengan path gambar
        Karyawan::create([
            'name' => 'Bayu Satrio',
            'tanggal_lahir' => '1999-06-06',
            'alamat' => 'Jl.Kesehatan No.10',
            'photo_url' => $path1 ? Storage::url($path1) : null,
            'status' => '1'
        ]);

        // Buat karyawan kedua dengan path gambar
        Karyawan::create([
            'name' => 'Marcella',
            'tanggal_lahir' => '1998-03-25',
            'alamat' => 'Jl.Bersih No.20',
            'photo_url' => $path2 ? Storage::url($path2) : null,
            'status' => '0'
        ]);
    }
}
