<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class SingleSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $userId = Auth::id();
            $sessionId = session()->getId();

            // Check if there's another session for this user
            $cachedSessionId = Cache::get('user-session-' . $userId);

            if ($cachedSessionId && $cachedSessionId !== $sessionId) {
                // Log out from the previous session
                session()->getHandler()->destroy($cachedSessionId);
            }

            // Store current session ID in cache
            Cache::put('user-session-' . $userId, $sessionId);
        }

        return $next($request);
    }
}
