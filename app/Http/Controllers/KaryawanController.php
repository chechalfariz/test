<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KaryawanRequest;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Repositories\Interfaces\KaryawanRepositoryInterface;

class KaryawanController extends Controller
{
    private $karyawanRepository;

    public function __construct(KaryawanRepositoryInterface $karyawanRepository)
    {
        $this->karyawanRepository = $karyawanRepository;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return $this->karyawanRepository->allKaryawans();
        }

        return view('pages.karyawan.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.karyawan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(KaryawanRequest $request)
    {
        $data = $request->all();
        if ($request->hasFile('photo_url')) {
            $path = $request->file('photo_url')->store('public/image');
            $filename = Storage::url($path);

            $data['photo_url'] = $filename;
        }
        $this->karyawanRepository->storeKaryawan($data);

        return redirect()->route('karyawan.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $karyawan = $this->karyawanRepository->findKaryawan($id);
        $umur = Carbon::parse($karyawan->tanggal_lahir)->diff(Carbon::now())->format('%y Tahun');

        return view('pages.karyawan.show', compact([
            'karyawan', 
            'umur'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $karyawan = $this->karyawanRepository->findKaryawan($id);

        return view('pages.karyawan.edit', compact('karyawan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(KaryawanRequest $request, string $id)
    {
        if($request->hasFile('photo_url')) {
            $path = $request->file('photo_url')->store('public/image');
            $filename = Storage::url($path);

            if($request->old_url) {
                Storage::delete(str_replace('/storage', '/public', $request->old_url));
            }
        } else {
            $filename = $request->old_url;
        }

        if(!isset($data['status'])) {
            $data['status'] = 0;
        }

        $data = $request->except(['photo_url', 'old_url']);
        $data['photo_url'] = $filename;
        $this->karyawanRepository->updateKaryawan($data, $id);

        return redirect()->route('karyawan.index')->with('message, Data Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->karyawanRepository->destroyKaryawan($id);

        return redirect()->route('karyawan.index')->with('status', 'Data Berhasil dihapus');
    }
}
