<?php

namespace App\Repositories\Interfaces;

interface KaryawanRepositoryInterface {
    public function allKaryawans();
    public function storeKaryawan($data);
    public function findKaryawan($id);
    public function updateKaryawan($data, $id);
    public function destroyKaryawan($id);
}