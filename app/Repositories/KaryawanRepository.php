<?php

namespace App\Repositories;

use App\Repositories\Interfaces\KaryawanRepositoryInterface;
use Carbon\Carbon;
use App\Models\Karyawan;
use Yajra\DataTables\Facades\DataTables;

class KaryawanRepository implements KaryawanRepositoryInterface
{
    public function allKaryawans()
    {
        $query = Karyawan::latest()->get();

        return DataTables::of($query)
        ->addColumn('action', function($item) {
            return '
                <a class="btn btn-warning btn-sm"
                    href="'. route('karyawan.show', $item->id) .'">
                    Show
                </a>
                <a class="btn btn-primary btn-sm"
                    href="'. route('karyawan.edit', $item->id) .'">
                    Edit
                </a>
                <form action="'. route('karyawan.destroy', $item->id) .'" method="POST" style="display:inline;">
                    ' . method_field('delete') . csrf_field() . '
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm(\'Are you sure?\')">
                        Delete
                    </button>
                </form>
            ';
        })
        ->addColumn('status', function($item){
            return $item->status ? 'Menikah' : 'Lajang';
        })
        ->addColumn('tanggal_lahir', function($item) {
            $umur = Carbon::parse($item->tanggal_lahir)->diffInYears(Carbon::now());
            return $item->tanggal_lahir . ' (' . $umur . ' Tahun)';
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function storeKaryawan($data)
    {
        return Karyawan::create($data);
    }

    public function findKaryawan($id)
    {
        return Karyawan::find($id);
    }

    public function updateKaryawan($data, $id)
    {
        $karyawan = Karyawan::where('id', $id)->first();
        $karyawan->name = $data['name'];
        $karyawan->tanggal_lahir = $data['tanggal_lahir'];
        $karyawan->alamat = $data['alamat'];
        $karyawan->photo_url = $data['photo_url'];
        $karyawan->status = $data['status'];
        $karyawan->save();
    }

    public function destroyKaryawan($id)
    {
        $karyawan = Karyawan::find($id);
        $karyawan->delete();
    }
}