<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name',
        'tanggal_lahir',
        'alamat',
        'photo_url',
        'status'
    ];
}
